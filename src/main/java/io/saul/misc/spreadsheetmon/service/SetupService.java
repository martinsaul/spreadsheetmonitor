package io.saul.misc.spreadsheetmon.service;

import io.saul.misc.spreadsheetmon.db.entity.Sheet;
import io.saul.misc.spreadsheetmon.db.entity.UpdateComparisonTask;
import io.saul.misc.spreadsheetmon.db.repository.MonitoringTaskRepository;
import io.saul.misc.spreadsheetmon.db.repository.SheetRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SetupService {

    private final MonitoringTaskRepository monitoringTaskRepository;
    private final SheetRepository sheetRepository;
    private int id = 1;

    @Autowired
    public SetupService(MonitoringTaskRepository monitoringTaskRepository, SheetRepository sheetRepository) {
        this.monitoringTaskRepository = monitoringTaskRepository;
        this.sheetRepository = sheetRepository;
        setup();
    }

    public void setup(){
        if(!sheetRepository.findAll().iterator().hasNext()){
            Sheet sheet = new Sheet();
            sheet.sheetId = "1U27V95kWlCVYWB0zye7DvqoXSkyqxgbA31eEJ_TKO6Y";
            sheetRepository.save(sheet);
        }

        monitoringTaskRepository.save(build(
                "Processing Time Comparison!C215:L215",
                "1U27V95kWlCVYWB0zye7DvqoXSkyqxgbA31eEJ_TKO6Y",
                "General Datapoint update!"
        ));

        monitoringTaskRepository.save(build(
                "September!O1:S2",
                "1U27V95kWlCVYWB0zye7DvqoXSkyqxgbA31eEJ_TKO6Y",
                "Days Estimated Days!"
        ));

        monitoringTaskRepository.save(build(
                "October!O1:S2",
                "1U27V95kWlCVYWB0zye7DvqoXSkyqxgbA31eEJ_TKO6Y",
                "October Estimated Days!"
        ));

        monitoringTaskRepository.save(build(
                "August!O1:S2",
                "1U27V95kWlCVYWB0zye7DvqoXSkyqxgbA31eEJ_TKO6Y",
                "August Estimated Days!"
        ));

        monitoringTaskRepository.save(build(
                "November!I1:M2",
                "1U27V95kWlCVYWB0zye7DvqoXSkyqxgbA31eEJ_TKO6Y",
                "November In Person Update!"
        ));

        monitoringTaskRepository.save(build(
                "October!I1:M2",
                "1U27V95kWlCVYWB0zye7DvqoXSkyqxgbA31eEJ_TKO6Y",
                "October In Person Update!"
        ));

        monitoringTaskRepository.save(build(
                "September!I1:M2",
                "1U27V95kWlCVYWB0zye7DvqoXSkyqxgbA31eEJ_TKO6Y",
                "September In Person Update!"
        ));

        monitoringTaskRepository.save(build(
                "August!I1:M2",
                "1U27V95kWlCVYWB0zye7DvqoXSkyqxgbA31eEJ_TKO6Y",
                "August In Person Update!")
        );

        monitoringTaskRepository.save(build(
                "July!I1:M2",
                "1U27V95kWlCVYWB0zye7DvqoXSkyqxgbA31eEJ_TKO6Y",
                "July In Person Update!")
        );

        monitoringTaskRepository.save(build(
                "June!I1:M2",
                "1U27V95kWlCVYWB0zye7DvqoXSkyqxgbA31eEJ_TKO6Y",
                "June In Person Update!")
        );

        monitoringTaskRepository.save(build(
                "May!I1:M2",
                "1U27V95kWlCVYWB0zye7DvqoXSkyqxgbA31eEJ_TKO6Y",
                "May In Person Update!")
        );

        monitoringTaskRepository.save(build(
                "April!I1:M2",
                "1U27V95kWlCVYWB0zye7DvqoXSkyqxgbA31eEJ_TKO6Y",
                "April In Person Update!")
        );

        monitoringTaskRepository.save(build(
                "March!I1:M2",
                "1U27V95kWlCVYWB0zye7DvqoXSkyqxgbA31eEJ_TKO6Y",
                "March In Person Update!")
        );

        monitoringTaskRepository.save(build(
                "February!I1:M2",
                "1U27V95kWlCVYWB0zye7DvqoXSkyqxgbA31eEJ_TKO6Y",
                "February In Person Update!")
        );

        monitoringTaskRepository.save(build(
                "January!I1:M2",
                "1U27V95kWlCVYWB0zye7DvqoXSkyqxgbA31eEJ_TKO6Y",
                "January In Person Update!")
        );

        monitoringTaskRepository.save(build(
                "November!G1:G2",
                "1U27V95kWlCVYWB0zye7DvqoXSkyqxgbA31eEJ_TKO6Y",
                "November AOR Update!"
        ));

        monitoringTaskRepository.save(build(
                "October!G1:G2",
                "1U27V95kWlCVYWB0zye7DvqoXSkyqxgbA31eEJ_TKO6Y",
                "October AOR Update!"
        ));

        monitoringTaskRepository.save(build(
                "September!G1:G2",
                "1U27V95kWlCVYWB0zye7DvqoXSkyqxgbA31eEJ_TKO6Y",
                "September AOR Update!"
        ));

        monitoringTaskRepository.save(build(
                "August!G1:G2",
                "1U27V95kWlCVYWB0zye7DvqoXSkyqxgbA31eEJ_TKO6Y",
                "August AOR Update!")
        );

        monitoringTaskRepository.save(build(
                "July!G1:G2",
                "1U27V95kWlCVYWB0zye7DvqoXSkyqxgbA31eEJ_TKO6Y",
                "July AOR Update!")
        );

        monitoringTaskRepository.save(build(
                "June!G1:G2",
                "1U27V95kWlCVYWB0zye7DvqoXSkyqxgbA31eEJ_TKO6Y",
                "June AOR Update!")
        );

        monitoringTaskRepository.save(build(
                "May!G1:G2",
                "1U27V95kWlCVYWB0zye7DvqoXSkyqxgbA31eEJ_TKO6Y",
                "May AOR Update!")
        );

        monitoringTaskRepository.save(build(
                "April!G1:G2",
                "1U27V95kWlCVYWB0zye7DvqoXSkyqxgbA31eEJ_TKO6Y",
                "April AOR Update!")
        );

        monitoringTaskRepository.save(build(
                "March!G1:G2",
                "1U27V95kWlCVYWB0zye7DvqoXSkyqxgbA31eEJ_TKO6Y",
                "March AOR Update!")
        );

        monitoringTaskRepository.save(build(
                "February!G1:G2",
                "1U27V95kWlCVYWB0zye7DvqoXSkyqxgbA31eEJ_TKO6Y",
                "February AOR Update!")
        );

        monitoringTaskRepository.save(build(
                "January!G1:G2",
                "1U27V95kWlCVYWB0zye7DvqoXSkyqxgbA31eEJ_TKO6Y",
                "January AOR Update!")
        );


        monitoringTaskRepository.save(build(
                "November!H1:H2",
                "1U27V95kWlCVYWB0zye7DvqoXSkyqxgbA31eEJ_TKO6Y",
                "November Processing Update!"
        ));

        monitoringTaskRepository.save(build(
                "October!H1:H2",
                "1U27V95kWlCVYWB0zye7DvqoXSkyqxgbA31eEJ_TKO6Y",
                "October Processing Update!"
        ));

        monitoringTaskRepository.save(build(
                "September!H1:H2",
                "1U27V95kWlCVYWB0zye7DvqoXSkyqxgbA31eEJ_TKO6Y",
                "September Processing Update!"
        ));

        monitoringTaskRepository.save(build(
                "August!H1:H2",
                "1U27V95kWlCVYWB0zye7DvqoXSkyqxgbA31eEJ_TKO6Y",
                "August Processing Update!")
        );

        monitoringTaskRepository.save(build(
                "July!H1:H2",
                "1U27V95kWlCVYWB0zye7DvqoXSkyqxgbA31eEJ_TKO6Y",
                "July Processing Update!")
        );

        monitoringTaskRepository.save(build(
                "June!H1:H2",
                "1U27V95kWlCVYWB0zye7DvqoXSkyqxgbA31eEJ_TKO6Y",
                "June Processing Update!")
        );

        monitoringTaskRepository.save(build(
                "May!H1:H2",
                "1U27V95kWlCVYWB0zye7DvqoXSkyqxgbA31eEJ_TKO6Y",
                "May Processing Update!")
        );

        monitoringTaskRepository.save(build(
                "April!H1:H2",
                "1U27V95kWlCVYWB0zye7DvqoXSkyqxgbA31eEJ_TKO6Y",
                "April Processing Update!")
        );

        monitoringTaskRepository.save(build(
                "March!H1:H2",
                "1U27V95kWlCVYWB0zye7DvqoXSkyqxgbA31eEJ_TKO6Y",
                "March Processing Update!")
        );

        monitoringTaskRepository.save(build(
                "February!H1:H2",
                "1U27V95kWlCVYWB0zye7DvqoXSkyqxgbA31eEJ_TKO6Y",
                "February Processing Update!")
        );

        monitoringTaskRepository.save(build(
                "January!H1:H2",
                "1U27V95kWlCVYWB0zye7DvqoXSkyqxgbA31eEJ_TKO6Y",
                "January Processing Update!")
        );

    }

    private UpdateComparisonTask build(String range, String spreadsheet, String description){
        UpdateComparisonTask task = new UpdateComparisonTask();

        task.id = id;
        task.description = description;
        task.range = range;
        task.spreadsheet = spreadsheet;
        id++;

        return task;
    }
}
