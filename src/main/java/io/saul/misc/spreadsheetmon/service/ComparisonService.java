package io.saul.misc.spreadsheetmon.service;

import io.saul.misc.spreadsheetmon.db.entity.SimplifiedRange;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class ComparisonService {

    private final static Logger log = LoggerFactory.getLogger(ComparisonService.class);

    public boolean compare(SimplifiedRange result, SimplifiedRange previous) {
        if(previous != null) {
            boolean updated = false;
            for (int index = 0; index < previous.getValues().size(); index++) {
                for (int subIndex = 0; subIndex < previous.getValues().get(index).size(); subIndex++) {
                    Object previousValue = previous.getValues().get(index).get(subIndex);
                    Object currentValue = result.getValues().get(index).get(subIndex);
                    if (!previousValue.equals(currentValue)) {
                        log.info("Update! Was: " + previousValue + " Now: " + currentValue);
                        updated = true;
                    }
                }
            }
            return updated;
        } else
            log.info("No data was present in database. Storing for first time.");
        return false;
    }
}
