package io.saul.misc.spreadsheetmon.service;

import io.saul.misc.spreadsheetmon.entity.ScheduledTask;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;

@Service
public class SchedulingService {
    private final static Logger log = LoggerFactory.getLogger(SchedulingService.class);


    private HashMap<Integer, ArrayList<ScheduledTask>> taskMap;
    private int count = 0;

    public SchedulingService(){
        taskMap = new HashMap<>();
    }

    public void schedule(int interval, ScheduledTask task){
        for(int period = 0; period < 60; period++)
            if(period % interval == 0)
                taskMap.computeIfAbsent(period, integer -> new ArrayList<>()).add(task);
    }

    @Scheduled(fixedDelay = 60000) //1 minute
    public void scheduleFixedDelayTask() {
        log.trace("Spring interval triggered.");
        ArrayList<ScheduledTask> scheduledTasks = taskMap.computeIfAbsent(count, integer -> new ArrayList<>());
        log.trace(MessageFormat.format("Number of pending scheduled tasks: {0} for interval: {1}", scheduledTasks.size(), count));
        for(ScheduledTask task: scheduledTasks){
            try {
                log.trace("Running Scheduled Job.");
                task.execute();
                log.trace("Scheduled Job complete.");
            } catch (Exception exception) {
                exception.printStackTrace();
            }
        }
        count++;
        count%=60;
    }

}
