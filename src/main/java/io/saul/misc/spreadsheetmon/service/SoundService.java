package io.saul.misc.spreadsheetmon.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.sound.sampled.*;

@Service
public class SoundService {
    private final static Logger log = LoggerFactory.getLogger(SoundService.class);

    private final ExecutorService executorService;
    private final HashMap<String, File> cache;

    public SoundService() {
        executorService = Executors.newCachedThreadPool();
        cache = new HashMap<>();
    }

    public void register(String name, File file){
        cache.put(name, file);
    }

    public void play(String name){
        File sound = cache.get(name);
        if(sound.exists())
            executorService.submit(new SoundThread(sound));
    }

    public static class SoundThread implements Runnable {

        private final File resource;

        public SoundThread(File resource) {
            this.resource = resource;
        }

        @Override
        public synchronized void run() {
            Clip clip = null;
            try {
                InputStream in = new FileInputStream(resource);
                InputStream bufferedIn = new BufferedInputStream(in);
                AudioInputStream stream = AudioSystem.getAudioInputStream(bufferedIn);
                AudioFormat format = stream.getFormat();
                DataLine.Info info = new DataLine.Info(Clip.class, format);
                long durationInMillis = 1000 * stream.getFrameLength() / (long) format.getFrameRate() + 100;
                clip = (Clip) AudioSystem.getLine(info);
                clip.open(stream);
                clip.addLineListener(new SimplifiedClipListener());
                clip.loop(0);
                wait(durationInMillis);
            } catch(InterruptedException ignore) {
                log.debug("Minor Warning: Audio playback interrupted.");
            } catch (Exception e) {
                log.error("Failed to play Sound due to exception.", e);
            } finally {
                try {
                    if(clip != null) {
                        clip.close();
                    }
                } catch(Exception x) {
                    log.error("Failed to safely close file.", x);
                }
            }
        }
    }

    private static class SimplifiedClipListener implements LineListener{
        @Override
        public void update(LineEvent event) {
            if(event.getType() == LineEvent.Type.STOP)
                notify();
        }
    }
}
