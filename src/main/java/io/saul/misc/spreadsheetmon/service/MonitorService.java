package io.saul.misc.spreadsheetmon.service;

import io.saul.misc.spreadsheetmon.db.entity.Sheet;
import io.saul.misc.spreadsheetmon.db.entity.SimplifiedRange;
import io.saul.misc.spreadsheetmon.db.entity.UpdateComparisonTask;
import io.saul.misc.spreadsheetmon.db.repository.MonitoringTaskRepository;
import io.saul.misc.spreadsheetmon.db.repository.SimplifiedRangeRepository;
import io.saul.misc.spreadsheetmon.entity.ScheduledTask;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;
import java.time.format.DateTimeFormatter;

@Service
public class MonitorService {

    private final static Logger log = LoggerFactory.getLogger(MonitorService.class);
    private static final DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");

    private final ComparisonService comparisonService;
    private final SimplifiedRangeRepository rangeRepository;
    private final SoundService soundService;
    private final MonitoringTaskRepository monitoringTaskRepository;
    private final SchedulingService schedulingService;
    private final SpreadsheetService spreadsheetService;

    @Autowired
    public MonitorService(
            ComparisonService comparisonService,
            SimplifiedRangeRepository rangeRepository,
            SoundService soundService,
            MonitoringTaskRepository monitoringTaskRepository,
            SchedulingService schedulingService,
            SetupService ignore,
            SpreadsheetService spreadsheetService){
        this.comparisonService = comparisonService;
        this.rangeRepository = rangeRepository;
        this.soundService = soundService;
        this.monitoringTaskRepository = monitoringTaskRepository;
        this.schedulingService = schedulingService;
        this.spreadsheetService = spreadsheetService;
        this.soundService.register("alert", new File("notification.wav"));
    }

    @PostConstruct
    private void setup(){
        for(Sheet sheet: spreadsheetService.getAll())
            schedulingService.schedule(1, new Task(sheet)); //TODO Move interval into Tasks.
    }

    private class Task extends ScheduledTask {
        private final Sheet sheet;

        public Task(Sheet sheet) {
            this.sheet = sheet;
        }

        @Override
        public void execute() {
            try {
                if(spreadsheetService.hasUpdated(sheet)){
                    log.info("Spreadsheet was modified.");
                    boolean detected;
                    detected = runUpdateComparisonTasks();
                    detected |= runCalculationComparisonTasks();
                    if(!detected)
                        log.info("No changes to tracked cells.");
                    spreadsheetService.unload(sheet);
                    spreadsheetService.updated(sheet);
                }
            } catch (IOException e) {
                log.error("Failed to retrieve spreadsheet and comparison.", e);
            }
        }

        private boolean runCalculationComparisonTasks() {
            // November!A10 -> F10 + <SHEET>!O1
            return false;
        }

        private boolean runUpdateComparisonTasks() throws IOException {
            boolean detected = false;
            for(UpdateComparisonTask task: monitoringTaskRepository.findAllBySpreadsheet(sheet.sheetId)){
                SimplifiedRange result = new SimplifiedRange(spreadsheetService.getLocalSheet(sheet).getRange(task.range));
                SimplifiedRange previous = rangeRepository.findFirstByRangeAndActiveIsTrueOrderByTimestamp(task.range) ;
                if(comparisonService.compare(result, previous)) {
                    soundService.play("alert");
                    log.info("New update for: " + task.description + "! Last modified: (" + dateFormatter.format(previous.timestamp) + ")");
                    detected = true;
                    rangeRepository.deactivateAllRange(previous.range);
                }
                rangeRepository.save(result);
            }
            return detected;
        }
    }
}
