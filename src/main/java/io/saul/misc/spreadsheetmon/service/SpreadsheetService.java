package io.saul.misc.spreadsheetmon.service;

import io.saul.misc.spreadsheetmon.db.entity.Sheet;
import io.saul.misc.spreadsheetmon.db.repository.SheetRepository;
import io.saul.misc.spreadsheetmon.offline.LocalSheet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.text.MessageFormat;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;

@Service
public class SpreadsheetService {
    private final static Logger log = LoggerFactory.getLogger(MonitorService.class);
    private final static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy_MM_dd-HHmm");

    private final SheetRepository sheetRepository;
    private final GoogleDriveService googleDriveService;
    private final File spreadsheetFolder;
    private final HashMap<String, LocalSheet> currentFile;

    public SpreadsheetService(SheetRepository sheetRepository, GoogleDriveService googleDriveService) {
        this.sheetRepository = sheetRepository;
        this.googleDriveService = googleDriveService;
        spreadsheetFolder = new File(new File("temp").getParentFile(), "temp");
        currentFile = new HashMap<>();
        //noinspection ResultOfMethodCallIgnored
        spreadsheetFolder.mkdirs();
    }

    public boolean hasUpdated(Sheet sheet) throws IOException {
        Sheet incoming = googleDriveService.getFileMetadata(sheet.sheetId);
        if (sheet.lastModified == null || sheet.lastModified.isBefore(incoming.lastModified)) {
            log.info("Spreadsheet meta information was modified.");

            getLocalSheet(incoming);

            sheet.lastModified = incoming.lastModified;
            return true;
        }
        log.trace("No changes to spreadsheet.");
        return false;
    }

    public LocalSheet getLocalSheet(Sheet sheet) throws IOException {
        LocalSheet localSheet = currentFile.get(sheet.sheetId);
        if(localSheet == null){
            File file = loadOrDownloadFile(sheet);
            log.debug("Reading spreadsheet file.");
            localSheet = new LocalSheet(file);
            log.debug("Spreadsheet loaded to memory successfully.");
            currentFile.put(sheet.sheetId, localSheet);
        }
        return localSheet;
    }

    private File loadOrDownloadFile(Sheet incoming) throws IOException {
        String timestamp = Long.toString(incoming.lastModified.toEpochSecond(ZoneOffset.UTC));
        String expectedFilename = MessageFormat.format("{0}_{1}_{2}.xlsx", incoming.sheetId, timestamp, formatter.format(incoming.lastModified));

        File file = new File(spreadsheetFolder, expectedFilename);

        if(!file.exists()) {
            log.debug("Downloading new spreadsheet from: " + incoming.exportURL);
            InputStream in = new URL(incoming.exportURL).openStream();
            Files.copy(in, file.toPath(), StandardCopyOption.REPLACE_EXISTING);
            log.debug("Download complete.");
        }
        return file;
    }

    public Iterable<Sheet> getAll() {
        return sheetRepository.findAll();
    }

    public void unload(Sheet sheet) {
        currentFile.get(sheet.sheetId).terminate();
        currentFile.remove(sheet.sheetId);
        log.debug("Releasing spreadsheet from memory.");
    }

    public void updated(Sheet sheet) {
        sheetRepository.save(sheet);
    }
}
