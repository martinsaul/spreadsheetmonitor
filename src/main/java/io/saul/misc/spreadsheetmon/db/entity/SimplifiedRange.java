package io.saul.misc.spreadsheetmon.db.entity;

import com.google.api.services.sheets.v4.model.ValueRange;
import io.saul.misc.spreadsheetmon.offline.RangeLookupResult;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Transient;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Entity
public class SimplifiedRange {
    @Id @GeneratedValue
    public long id;
    public String range;
    public String value;
    public LocalDateTime timestamp;
    public boolean active;

    @Transient
    public List<List<Object>> cache = null;

    public SimplifiedRange(){}

    public SimplifiedRange(ValueRange valueRange){
        range = valueRange.getRange().replace("'", "");
        cache = valueRange.getValues();
        active = true;
        timestamp = LocalDateTime.now();
        parseCache();
    }

    public SimplifiedRange(RangeLookupResult data){
        range = data.range;
        cache = data.value;
        timestamp = LocalDateTime.now();
        active = true;
        parseCache();
    }

    public List<List<Object>> getValues() {
        if(cache == null){
            cache = new ArrayList<>();
            for(String val: value.split("[|]")){
                ArrayList<Object> list = new ArrayList<>(Arrays.asList(val.split("-")));
                cache.add(list);
            }
            timestamp = LocalDateTime.now();
        }
        return cache;
    }

    private void parseCache() {
        List<String> temp = new ArrayList<>();
        for(List<Object> row: cache){
            temp.add(row
                    .stream()
                    .map(String::valueOf)
                    .collect(Collectors.joining("-")));
        }
        value = temp.stream().map(String::valueOf).collect(Collectors.joining("|"));
    }

}
