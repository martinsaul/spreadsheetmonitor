package io.saul.misc.spreadsheetmon.db.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.time.LocalDateTime;

@Entity
public class Sheet {
    @Id @GeneratedValue(strategy = GenerationType.AUTO)
    public int id;

    public String sheetId;
    public String exportURL;
    public LocalDateTime lastModified;
}
