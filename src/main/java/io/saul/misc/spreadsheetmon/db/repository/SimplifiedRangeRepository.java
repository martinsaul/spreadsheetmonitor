package io.saul.misc.spreadsheetmon.db.repository;

import io.saul.misc.spreadsheetmon.db.entity.SimplifiedRange;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

public interface SimplifiedRangeRepository extends Repository<SimplifiedRange, Long>, CrudRepository<SimplifiedRange, Long> {
    SimplifiedRange findFirstByRangeAndActiveIsTrueOrderByTimestamp(String range);

    @Transactional
    @Modifying
    @Query("update SimplifiedRange c set c.active = false WHERE c.range = :range")
    void deactivateAllRange(@Param("range") String range);

}
