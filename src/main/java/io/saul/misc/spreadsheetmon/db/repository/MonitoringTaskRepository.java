package io.saul.misc.spreadsheetmon.db.repository;

import io.saul.misc.spreadsheetmon.db.entity.UpdateComparisonTask;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.Repository;

import java.util.List;

public interface MonitoringTaskRepository extends Repository<UpdateComparisonTask, Long>, CrudRepository<UpdateComparisonTask, Long> {
    List<UpdateComparisonTask> findAllBySpreadsheet(String spreadsheet);
}
