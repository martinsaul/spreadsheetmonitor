package io.saul.misc.spreadsheetmon.db.repository;

import io.saul.misc.spreadsheetmon.db.entity.Sheet;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.Repository;

public interface SheetRepository extends Repository<Sheet, Long>, CrudRepository<Sheet, Long> {
}
