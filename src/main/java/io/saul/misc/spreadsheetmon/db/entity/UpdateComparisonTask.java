package io.saul.misc.spreadsheetmon.db.entity;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class UpdateComparisonTask {
    @Id
    public int id;

    public String description;
    public String spreadsheet;
    public String range;

    public void setSpreadsheet(String spreadsheet) {
        this.spreadsheet = spreadsheet;
    }

    public void setRange(String range) {
        this.range = range;
    }
}
