package io.saul.misc.spreadsheetmon;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.io.File;
import java.io.IOException;
import java.util.Set;

@EnableScheduling
@SpringBootApplication
public class Launcher {

    private final static Logger log = LoggerFactory.getLogger(Launcher.class);

    public static void main(String [] parms) throws IOException {
        Set<String> parameters = Set.of(parms);

        if(parameters.contains("-wipe") || parameters.contains("-wipeData")){
            log.info("Wiping data folder.");
            FileUtils.deleteDirectory(new File("data"));
        }

        if(parameters.contains("-wipe") || parameters.contains("-wipeTemp")){
            log.info("Wiping temp folder");
            FileUtils.deleteDirectory(new File("temp"));
        }

        SpringApplication.run(Launcher.class, parms);
    }

}
