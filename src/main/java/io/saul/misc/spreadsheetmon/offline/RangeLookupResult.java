package io.saul.misc.spreadsheetmon.offline;

import java.util.List;

public class RangeLookupResult {
    public String range;
    public List<List<Object>> value;
}
