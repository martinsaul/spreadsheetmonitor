package io.saul.misc.spreadsheetmon.offline;

import org.apache.poi.ss.SpreadsheetVersion;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.AreaReference;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class LocalSheet {
    private final static Logger log = LoggerFactory.getLogger(LocalSheet.class);

    private final File file;
    private final Workbook wb;


    public LocalSheet(File file) throws IOException {
        this.file = file;
        this.wb = new XSSFWorkbook(new FileInputStream(file));
    }

    public RangeLookupResult getRange(String range) {
        RangeLookupResult result = new RangeLookupResult();
        result.range = range;
        range = safeRange(range);
        CellReference[] cellRefList = new AreaReference(range, SpreadsheetVersion.EXCEL2007).getAllReferencedCells();

        List<List<Object>> values = new ArrayList<>();
        ArrayList<Object> currentRow = null;
        int rowId = -1;
        for (CellReference cellRef : cellRefList) {
            Sheet s = wb.getSheet(cellRef.getSheetName());
            Row row = s.getRow(cellRef.getRow());
            if(row.getRowNum() != rowId){
                rowId = row.getRowNum();
                if(currentRow != null)
                    values.add(currentRow);
                currentRow = new ArrayList<>();
            }

            Cell c = row.getCell(cellRef.getCol());

            switch (c.getCellType()){
                case NUMERIC -> currentRow.add(Double.toString(c.getNumericCellValue()));
                case STRING -> currentRow.add(c.getStringCellValue());
                case BOOLEAN -> currentRow.add(Boolean.toString(c.getBooleanCellValue()));
                case FORMULA -> {
                    switch(c.getCachedFormulaResultType()) {
                        case NUMERIC -> currentRow.add(Double.toString(c.getNumericCellValue()));
                        case STRING -> currentRow.add(c.getStringCellValue());
                        case BOOLEAN -> currentRow.add(Boolean.toString(c.getBooleanCellValue()));
                        case _NONE, BLANK, ERROR -> currentRow.add("");
                    }
                }
                case _NONE, BLANK, ERROR -> currentRow.add("");
            }
        }
        if(currentRow != null)
            values.add(currentRow);

        result.value = values;

        return result;
    }

    private String safeRange(String range) {
        String result = range;
        if(range.contains("!")){
            String[] compositeRange = range.split("!");
            String sheet = compositeRange[0];
            String rg = compositeRange[1];
            if(sheet.contains(" "))
                result = "'" + sheet + "'!" + rg;
        }

        return result;
    }

    public void terminate() {
        try {
            wb.close();
        } catch (IOException e) {
            log.error("Error closing spreadsheet.");
        }
    }
}
