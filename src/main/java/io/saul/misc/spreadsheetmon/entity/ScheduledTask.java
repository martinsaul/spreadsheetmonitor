package io.saul.misc.spreadsheetmon.entity;

public abstract class ScheduledTask {
    public abstract void execute() throws Exception;
}
